terraform {
  backend "s3" {
    bucket = "state-management-indacloud-run2"
    key    = "test-stack/state/terraform.tfstate"
    region = "eu-central-1"
    profile = "indacloud"
    dynamodb_table = "indacloud_lock_table_run2"
  }
}

data "terraform_remote_state" "my_stack" {
  backend = "s3"
  config = {
    profile = "indacloud"
    bucket = "state-management-indacloud-run2"
    key    = "test-stack/state/terraform.tfstate"
    region = "eu-central-1"
  }
}