data "aws_ami" "aws_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"] # AWS
}

resource "aws_instance" "ec2_instance" {
  ami           = "${data.aws_ami.aws_linux.id}"
  instance_type = "t2.micro"
}

resource "aws_instance" "ec2_instance2" {
  ami           = "${data.aws_ami.aws_linux.id}"
  instance_type = "t2.micro"
}

resource "aws_s3_bucket" "change_bucket" {
  bucket = "test-change-bucket-indacloud"
  acl = "private"
}
